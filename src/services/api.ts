import axios from "axios";

const api = axios.create({
  //baseURL: "http://192.168.1.71:3333",// leiria
  //baseURL: "http://194.100.100.118:3333", // leiria trabalho
  baseURL: "http://192.168.1.68:3333", // cernache
});

export default api;

//json-server ./src/services/server.json --host 192.168.1.68 --port 3333 --delay 1400
